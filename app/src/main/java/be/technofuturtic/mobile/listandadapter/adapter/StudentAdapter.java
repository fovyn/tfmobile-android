package be.technofuturtic.mobile.listandadapter.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import be.technofuturtic.mobile.listandadapter.EditAction;
import be.technofuturtic.mobile.listandadapter.R;
import be.technofuturtic.mobile.listandadapter.model.Student;

public class StudentAdapter extends ArrayAdapter<Student> {
    private EditAction<Student> editAction;

    public StudentAdapter(@NonNull Context context, List<Student> students, EditAction<Student> editAction) {
        // List.toArray => renvoi Object[]
        // Or le constructeur de ArrayAdapter<Student> demande Student[]
        super(context, R.layout.adapter_student, students);

        this.editAction = editAction;
    }

    @NonNull
    @Override
    //Méthode permettant la création d'un vue dans la list
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.adapter_student, parent, false);
        }

        Student student = this.getItem(position);

        TextView tvFirstName = convertView.findViewById(R.id.adapter_student_firstName);
        TextView tvLastName = convertView.findViewById(R.id.adapter_student_lastName);
        Button btnEdit = convertView.findViewById(R.id.adapter_student_btnEdit);

        tvFirstName.setText(student.getFirstName());
        tvLastName.setText(student.getLastName());

        btnEdit.setOnClickListener(v -> editAction.execute(student));

        return convertView;
    }
}
