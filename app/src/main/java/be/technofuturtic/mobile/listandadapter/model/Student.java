package be.technofuturtic.mobile.listandadapter.model;

import java.time.LocalDate;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@EqualsAndHashCode(of = {"noma", "lastName"})
@ToString(of = {"noma", "lastName", "firstName", "email", "birthDate"})
@NoArgsConstructor
@AllArgsConstructor
public class Student {
    private String noma;
    private String lastName;
    private String firstName;
    private String email;
    private LocalDate birthDate;
}
