package be.technofuturtic.mobile.listandadapter;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private Button btnList;
    private Button btnRecycler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.btnList = findViewById(R.id.main_btn_list);
        this.btnRecycler = findViewById(R.id.main_btn_recycler);


        //Valid si les attributs btnList && btnRecycler ne sont pas null
        assert this.btnList != null;
        assert this.btnRecycler != null;

        this.btnList.setOnClickListener(this);
        this.btnRecycler.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.main_btn_list: goToListActivity(); break;
            case R.id.main_btn_recycler: goToRecyclerActivity(); break;
        }
    }

    private void goToListActivity() {
        Intent changeActivityIntent = new Intent(MainActivity.this, ListActivity.class);
        startActivity(changeActivityIntent);
    }

    private void goToRecyclerActivity() {
        Intent changeActivityIntent = new Intent(MainActivity.this, ListActivity.class);
        startActivity(changeActivityIntent);
    }
}