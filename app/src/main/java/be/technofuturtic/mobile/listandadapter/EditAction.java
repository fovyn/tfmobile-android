package be.technofuturtic.mobile.listandadapter;

@FunctionalInterface
public interface EditAction<T> {
    void execute(T item);
}
