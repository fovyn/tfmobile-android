package be.technofuturtic.mobile.listandadapter;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.google.common.collect.Lists;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import be.technofuturtic.mobile.listandadapter.adapter.StudentAdapter;
import be.technofuturtic.mobile.listandadapter.model.Student;

public class ListActivity extends AppCompatActivity {
    private ListView lvMovie;
    private Button btnAddItem;

    private List<Student> students;

    private StudentAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        this.lvMovie = findViewById(R.id.list_lv_movie);
        this.btnAddItem = findViewById(R.id.list_btn_add_item);

        this.students = new ArrayList<>();
    }

    @Override
    protected void onResume() {
        super.onResume();

        this.students.add(new Student("IRAM-0001", "Ovyn", "Flavian", "flavian.ovyn@iram.be", LocalDate.of(1991, 7, 19)));
        this.students.add(new Student("IRAM-0002", "Ovyn", "Flavian", "flavian.ovyn@iram.be", LocalDate.of(1991, 7, 19)));
        this.students.add(new Student("IRAM-0003", "Ovyn", "Flavian", "flavian.ovyn@iram.be", LocalDate.of(1991, 7, 19)));
        this.students.add(new Student("IRAM-0004", "Ovyn", "Flavian", "flavian.ovyn@iram.be", LocalDate.of(1991, 7, 19)));
        this.students.add(new Student("IRAM-0005", "Ovyn", "Flavian", "flavian.ovyn@iram.be", LocalDate.of(1991, 7, 19)));
        this.students.add(new Student("IRAM-0006", "Ovyn", "Flavian", "flavian.ovyn@iram.be", LocalDate.of(1991, 7, 19)));
        this.students.add(new Student("IRAM-0007", "Ovyn", "Flavian", "flavian.ovyn@iram.be", LocalDate.of(1991, 7, 19)));
        this.students.add(new Student("IRAM-0008", "Ovyn", "Flavian", "flavian.ovyn@iram.be", LocalDate.of(1991, 7, 19)));
        this.students.add(new Student("IRAM-0009", "Ovyn", "Flavian", "flavian.ovyn@iram.be", LocalDate.of(1991, 7, 19)));
        this.students.add(new Student("IRAM-0010", "Ovyn", "Flavian", "flavian.ovyn@iram.be", LocalDate.of(1991, 7, 19)));


        adapter = new StudentAdapter(ListActivity.this, this.students, this::goToEdit);

        this.lvMovie.setAdapter(adapter);

    }

    private void goToEdit(Student student) {
        Log.d("STUDENT", String.format("Student clicked => %s", student));
    }


}